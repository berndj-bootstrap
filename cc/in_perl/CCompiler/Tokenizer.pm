package CCompiler::Tokenizer;

use Exporter qw(import);

our @EXPORT = qw(EOF NUMBER IDENTIFIER TYPENAME LROUND RROUND COMMA);

# Syntactic classes.
sub EOF { return 0; }
sub NUMBER { return 1; }
sub IDENTIFIER { return 2; }
sub TYPENAME { return 3; }
sub LROUND { return 4; }
sub RROUND { return 5; }
sub COMMA { return 6; }
sub SEMICOLON { return 7; }

sub signal_fixed_tokens {
	my ($self) = @_;

	# FIXED_TOKENS should be ordered from longest to shortest, so the longest match wins the regex.
	$self->{FIXED_TOKENS} = [ sort { length($b->[0]) <=> length($a->[0]); } @{$self->{FIXED_TOKENS}} ];

	my $regex = '^(';
	# IDENTIFIER.
	$regex .= '([a-zA-Z_][a-zA-Z0-9_]*)';
	# NUMBER.
	$regex .= '|(0x[0-9a-fA-F]+|0[0-7]*|[1-9][0-9]*)';
	# Capture the remaining input.
	$regex .= ')(.*)$';

	$self->{REGEX} = qr/$regex/s;

	return $self;
}

sub new {
	my ($class, $input) = @_;
	my $self = {
		INPUT => $input,
		FIXED_TOKENS => [
			[ 'int' => TYPENAME ],
			[ 'char' => TYPENAME ],
			[ '(' => LROUND ],
			[ ')' => RROUND ],
			[ ',' => COMMA ],
			[ ';' => SEMICOLON ],
		],
		REGEX => undef,
		WHITESPACE => qr{^(?:\s|\/\*(?:(?:[^*]|\*[^/])*?)\*\/)*(.*)$}s,
	};

	signal_fixed_tokens($self);

	bless $self, $class;
	return $self;
}

sub token {
	my ($self) = @_;

	my $input = $self->{INPUT};

	# Ignore whitespace.
	($input) = ($input =~ $self->{WHITESPACE});
	if ($input eq '') {
		return CCompiler::Token->new(EOF, '');
	}

	my ($value, $identifier, $number, $rest) = ($input =~ $self->{REGEX});
	my $value_len = defined($value) ? length($value) : 0;

	my $syntactic_class = undef;
	$syntactic_class = IDENTIFIER if defined $identifier;
	$syntactic_class = NUMBER if defined $number;

	# Look for fixed tokens that are longer than what we already have.
	FIXED: for (my $i = 0; $i < @{$self->{FIXED_TOKENS}}; $i++) {
		my $candidate = $self->{FIXED_TOKENS}->[$i];
		my $len = length($candidate->[0]);
		if ($len < $value_len) {
			# Already have a longer wildcard token.
			last FIXED;
		}
		if (substr($input, 0, $len) eq $candidate->[0]) {
			# Oops, found a fixed token that also matches; fix up the token parameters.
			$value = $candidate->[0];
			$syntactic_class = $candidate->[1];
			$rest = substr($input, $len);
			last FIXED;
		}
	}

	$self->{INPUT} = $rest;

	if (!defined($syntactic_class)) {
		return undef;
	}

	return CCompiler::Token->new($syntactic_class, $value);
}

package CCompiler::Token;

sub new {
	my ($class, $syntactic_class, $value) = @_;
	my $self = {
		CLASS => $syntactic_class,
		VALUE => $value,
	};

	bless $self, $class;
	return $self;
}

sub class {
	my ($self) = @_;

	return $self->{CLASS};
}

sub represent {
	my ($self) = @_;

	return "$self->{VALUE} ($self->{CLASS})";
}

1;
