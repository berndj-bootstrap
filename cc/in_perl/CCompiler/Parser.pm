package CCompiler::Parser;

use CCompiler::Tokenizer;

sub new {
	my ($class, $input) = @_;
	my $self = {
		INPUT => $input,
		TOKENIZER => CCompiler::Tokenizer->new($input),
	};

	bless $self, $class;
	return $self;
}

sub parse {
	my ($self) = @_;
	my $token = undef; # An empty file is not a valid C program.

	TOKEN: while ($token = $self->{TOKENIZER}->token) {
		my $class = $token->class;
		last TOKEN if $class == EOF;
		print "token: " . $token->represent . "\n";
	}

	$token or die "Parser error";
}

my $program = <<EOF;
int abs(int j);
EOF
#{
#	local $/ = undef;
#	$program = <>;
#}

CCompiler::Parser->new($program)->parse;

1;
