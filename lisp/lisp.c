#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct environment;
struct pair;
struct stack_slot;

struct charseq {
	size_t len;
	char *s;
	int gc;
};

union value_pointer {
	int vp_int;
	int vp_pair;
	int vp_charseq;
	union value_pointer (*vp_builtin)(struct stack_slot *top_of_stack,
					  struct environment *env,
					  union value_pointer rest);
	char const *vp_opaque;
};

struct binding {
	union value_pointer name;
	union value_pointer value;
	int resource_num;
};

struct resource_pool {
	void **resources;
	int resources_used;
	int resources_size;
	size_t alloc_size;
};

struct environment {
	struct resource_pool variables;
	struct environment *parent;
	int resource_num;
};

struct pair {
	union value_pointer car;
	union value_pointer cdr;
	int gc;
};

struct stack_slot {
	struct environment *env;
	union value_pointer rest;
	union value_pointer retval;
	union value_pointer function;
	union value_pointer sexpr;
	union value_pointer args;
	union value_pointer list;
	union value_pointer p;
	union value_pointer subeval_arg;
	union value_pointer *arglink;
	struct environment *call_env;
	int gc;
	int resource_num;
	int return_path;
	int evaluating;
	struct stack_slot *prev;
};

void *resource_get(struct resource_pool *rp, int i);
union value_pointer builtin_lambda(struct stack_slot *top_of_stack,
				   struct environment *env,
				   union value_pointer rest);
void gc_protect(union value_pointer v);
union value_pointer gc_unprotect(union value_pointer v);

char const opaque_builtin_function[] = "builtin-function";
char const opaque_builtin_macro[] = "builtin-macro";
char const opaque_user_function[] = "user-function";
char const opaque_user_macro[] = "user-macro";
union value_pointer constant_true, constant_false, constant_nil;

struct resource_pool bindings, charseqs, environments, pairs, stack_slots;

void pointer_mark(union value_pointer *v, int type)
{
	int pointer_bits = v->vp_int;

	pointer_bits &= ~3;
	pointer_bits |= type;

	v->vp_int = pointer_bits;
}

int pointer_type(union value_pointer v)
{
	return (v.vp_int & 3);
}

int get_integer(union value_pointer v)
{
	return (v.vp_int >> 2);
}

struct pair *get_pair(union value_pointer v)
{
	return resource_get(&pairs, v.vp_pair >> 2);
}

struct charseq *get_charseq(union value_pointer v)
{
	return resource_get(&charseqs, v.vp_charseq >> 2);
}

char *get_charseq_chars(union value_pointer v)
{
	struct charseq *cs;

	cs = get_charseq(v);

	return cs->s;
}

size_t get_charseq_len(union value_pointer v)
{
	struct charseq *cs;

	cs = get_charseq(v);

	return cs->len;
}

void resource_pool_init(struct resource_pool *rp, size_t sz)
{
	rp->resources = NULL;
	rp->resources_used = 0;
	rp->resources_size = 0;
	rp->alloc_size = sz;
}

void resource_pool_free(struct resource_pool *rp)
{
	int i;

	for (i = 0; i < rp->resources_used; i++) {
		free(rp->resources[i]);
	}
	free(rp->resources);
}

int resource_add(struct resource_pool *rp, void *r)
{
	int i;

	if (rp->resources_used >= rp->resources_size) {
		int newsize = rp->resources_used * 2 + 16;

		for (i = 0; i < rp->resources_used; i++) {
			if (rp->resources[i] == NULL) {
				rp->resources[i] = r;
				return i;
			}
		}

		rp->resources = realloc(rp->resources,
					sizeof (*rp->resources) * newsize);
		rp->resources_size = newsize;
	}

	i = rp->resources_used++;
	rp->resources[i] = r;

	return i;
}

int resource_count(struct resource_pool *rp)
{
	return rp->resources_used;
}

void *resource_get(struct resource_pool *rp, int i)
{
	return rp->resources[i];
}

void resource_free(struct resource_pool *rp, int i)
{
	free(rp->resources[i]);
	rp->resources[i] = NULL;
}

int resource_new_i(struct resource_pool *rp)
{
	return resource_add(rp, malloc(rp->alloc_size));
}

struct environment *environment_new(struct environment *parent)
{
	struct environment *env;
	int envnum;

	envnum = resource_new_i(&environments);
	env = resource_get(&environments, envnum);
	env->resource_num = envnum;
	resource_pool_init(&env->variables, 0);
	env->parent = parent;

	return env;
}

void environment_free(struct environment *env)
{
	resource_pool_free(&env->variables);
	resource_free(&environments, env->resource_num);
}

union value_pointer charseq_new(char const *s, size_t len, size_t size, int kind)
{
	union value_pointer retval;
	struct charseq *cs;

	if (kind == 1) {
		int i;

		for (i = 0; i < resource_count(&charseqs); i++) {
			cs = resource_get(&charseqs, i);
			if (cs && len == cs->len && strncmp(s, cs->s, len) == 0) {
				retval.vp_charseq = i << 2;
				pointer_mark(&retval, kind);

				return retval;
			}
		}
	}

	retval.vp_charseq = resource_new_i(&charseqs) << 2;
	pointer_mark(&retval, kind);

	cs = get_charseq(retval);
	cs->len = size;
	cs->s = malloc(size + 1);
	memcpy(cs->s, s, len);
	cs->s[len] = 0;
	cs->gc = 2;

	return retval;
}

union value_pointer pair_new(void)
{
	union value_pointer retval;

	retval.vp_pair = resource_new_i(&pairs) << 2;
	pointer_mark(&retval, 3);
	get_pair(retval)->cdr = constant_nil;
	get_pair(retval)->gc = 2;

	return retval;
}

union value_pointer lisp_car(union value_pointer v)
{
	return get_pair(v)->car;
}

union value_pointer lisp_cdr(union value_pointer v)
{
	return get_pair(v)->cdr;
}

int lisp_length(union value_pointer v)
{
	int n;

	n = 0;
	while (pointer_type(v) == 3 && get_pair(v) != get_pair(constant_nil)) {
		n++;
		v = lisp_cdr(v);
	}

	return n;
}

union value_pointer lisp_cons(union value_pointer left, union value_pointer right)
{
	union value_pointer retval;

	retval = pair_new();
	get_pair(retval)->car = left;
	get_pair(retval)->cdr = right;
	pointer_mark(&retval, 3);

	if (left.vp_opaque != opaque_builtin_function &&
	    left.vp_opaque != opaque_builtin_macro) {
		if (left.vp_opaque != opaque_user_function &&
		    left.vp_opaque != opaque_user_macro) {
			gc_unprotect(left);
		}
		gc_unprotect(right);
	}

	return retval;
}

union value_pointer lisp_integer(int n)
{
	union value_pointer retval;

	retval.vp_int = n << 2;
	pointer_mark(&retval, 0);

	return retval;
}

union value_pointer lisp_opaque(char const *cookie)
{
	union value_pointer retval;

	retval.vp_opaque = cookie;

	return retval;
}

union value_pointer lisp_builtin(union value_pointer (*builtin)(struct stack_slot *top_of_stack,
								struct environment *env,
								union value_pointer rest))
{
	union value_pointer retval;

	retval.vp_builtin = builtin;

	return retval;
}

struct binding *binding_new(struct environment *env,
			    char const *name,
			    union value_pointer value)
{
	struct binding *b;
	int bnum;

	bnum = resource_new_i(&bindings);
	b = resource_get(&bindings, bnum);
	b->resource_num = bnum;
	b->name = charseq_new(name, strlen(name), strlen(name), 1);
	b->value = value;

	resource_add(&env->variables, b);

	gc_unprotect(b->name);
	gc_unprotect(b->value);

	return b;
}

union value_pointer builtin_new(char const *opaque_type,
				union value_pointer (*builtin)(struct stack_slot *top_of_stack,
							       struct environment *env,
							       union value_pointer rest))
{
	return lisp_cons(lisp_opaque(opaque_type),
			 lisp_builtin(builtin));
}

size_t parse_word(union value_pointer *vp, char const *buf, size_t len)
{
	size_t i;
	int n;

	for (n = 0, i = 0; i < len; i++) {
		if (buf[i] < '0' || buf[i] > '9') {
			break;
		}
		n = n*10 + buf[i] - '0';
	}

	if (i < len) {
		char *symbol;

		switch (buf[i]) {
		case ')':
		case ' ':
		case '\n':
		case '\t':
			break;
		default:
			for (i++; i < len; i++) {
				switch (buf[i]) {
				case ')':
				case ' ':
				case '\n':
				case '\t':
					len = i + 1;
					break;
				default:
					continue;
				}
				break;
			}
			*vp = charseq_new(buf, i, i, 1);
			gc_unprotect(*vp);
			return i;
		}
	}
	vp->vp_int = n << 2;
	pointer_mark(vp, 0);
	return i;
}

size_t parse_string(union value_pointer *vp, char const *buf, size_t len)
{
	size_t i, n;
	char *s;
	int backslash;

	for (backslash = n = 0, i = 1; i < len; i++) {
		if (backslash) {
			n++;
			backslash = 0;
		} else if (buf[i] == '"') {
			break;
		} else if (buf[i] == '\\') {
			backslash = 1;
		} else {
			n++;
		}
	}
	len = i;

	*vp = charseq_new(buf + 1, n, n, 2);
	gc_unprotect(*vp);

	for (backslash = 0, i = 1, s = get_charseq_chars(*vp); i < len; i++) {
		if (backslash) {
			switch (buf[i]) {
			case 'n':
				*s++ = '\n';
				break;
			default:
				*s++ = buf[i];
				break;
			}
			backslash = 0;
		} else if (buf[i] == '\\') {
			backslash = 1;
		} else {
			*s++ = buf[i];
		}
	}

	return i + 1;
}

size_t parse_forms(char const *buf, size_t len, union value_pointer *form)
{
	size_t i, n;
	int improper = 0;

	for (i = 0; i < len; i++) {
		char const c = buf[i];
		union value_pointer u, v;

		v = constant_nil;

		switch (c) {
		case '(':
			u = constant_nil;
			n = parse_forms(buf + i + 1, len - i - 1, &u) + 1;
			i += n;
			if (improper) {
				*form = u;
			} else {
				v = lisp_cons(u, constant_nil);
				*form = v;
				gc_unprotect(v);
				form = &get_pair(*form)->cdr;
			}
			break;
		case ')':
		case ' ':
		case '\n':
		case '\t':
			break;
		case '"':
			v = pair_new();
			n = parse_string(&get_pair(v)->car, buf + i, len - i);
			i += n - 1;
			*form = v;
			gc_unprotect(v);
			pointer_mark(form, 3);
			form = &get_pair(v)->cdr;
			break;
		default:
			n = parse_word(&u, buf + i, len - i);
			if (pointer_type(u) == 1 &&
			    strcmp(get_charseq_chars(u), ".") == 0) {
				improper = 1;
			}
			if (improper) {
				*form = u;
			} else {
				*form = lisp_cons(u, constant_nil);
				gc_unprotect(*form);
				form = &get_pair(*form)->cdr;
			}
			gc_unprotect(u);
			i += n - 1;
			break;
		}

		if (c == ')') {
			break;
		}
	}

	return i;
}

void pretty_print(union value_pointer v)
{
	union value_pointer i;
	struct pair *p;
	char *s;
	int j;

	switch (pointer_type(v)) {
	case 0:
		printf("%d", get_integer(v));
		break;
	case 1:
		printf("%s", get_charseq_chars(v));
		break;
	case 2:
		s = get_charseq_chars(v);
		printf("\"");
		for (j = 0; j < get_charseq_len(v); j++) {
			switch (s[j]) {
			case '"':
				printf("\\");
			default:
				printf("%c", s[j]);
				break;
			}
		}
		printf("\"");
		break;
	case 3:
		p = get_pair(v);
		if (p != get_pair(constant_nil) &&
		    (p->car.vp_opaque == opaque_builtin_function ||
		     p->car.vp_opaque == opaque_builtin_macro ||
		     p->car.vp_opaque == opaque_user_function ||
		     p->car.vp_opaque == opaque_user_macro)) {
			printf("%s", p->car.vp_opaque);
			break;
		}

		printf("(");
		for (i = v;
		     pointer_type(i) == 3 && get_pair(i) != get_pair(constant_nil);
		     i = lisp_cdr(i)) {
			if (i.vp_pair != v.vp_pair) {
				printf(" ");
			}
			pretty_print(lisp_car(i));
		}
		if (pointer_type(i) != 3) {
			printf(" . ");
			pretty_print(i);
		}
		printf(")");
		break;
	}
}

struct stack_slot *stack_call(struct stack_slot *top_of_stack,
			      struct environment *env,
			      union value_pointer rest)
{
	struct stack_slot *slot;
	int slotnum;

	slotnum = resource_new_i(&stack_slots);
	slot = resource_get(&stack_slots, slotnum);
	slot->resource_num = slotnum;
	slot->evaluating = 1;
	slot->return_path = 4040;
	slot->env = env;
	slot->rest = rest;
	slot->prev = top_of_stack;
	slot->gc = 2;

	return slot;
}

struct stack_slot *stack_return(struct stack_slot *top_of_stack)
{
	struct stack_slot *slot;

	slot = top_of_stack->prev;

	resource_free(&stack_slots, top_of_stack->resource_num);

	return slot;
}

union value_pointer eval_symbol(struct environment *env, union value_pointer sym)
{
	char const *symbol_name;
	int i;

	symbol_name = get_charseq_chars(sym);
	do {
		for (i = 0; i < resource_count(&env->variables); i++) {
			struct binding *b;

			b = resource_get(&env->variables, i);
			if (strcmp(get_charseq_chars(b->name), symbol_name) == 0) {
				return b->value;
			}
		}
		env = env->parent;
	} while (env != NULL);

	return constant_nil;
}

union value_pointer builtin_eval(struct stack_slot *caller,
				 struct environment *env_toplevel,
				 union value_pointer rest_toplevel)
{
	struct stack_slot *top_of_stack;
	union value_pointer element;
	union value_pointer consequent, alternate;
	union value_pointer retval;
	struct pair *closure;

	top_of_stack = stack_call(caller, env_toplevel, rest_toplevel);
	top_of_stack->return_path = 0;

	while (1) {
		if (top_of_stack->evaluating) {
			switch (pointer_type(lisp_car(top_of_stack->rest))) {
			case 0:
			case 2:
				top_of_stack->retval = lisp_car(top_of_stack->rest);
				break;
			case 1:
				top_of_stack->retval = eval_symbol(top_of_stack->env,
								   lisp_car(top_of_stack->rest));
				break;
			case 3:
				top_of_stack->list = lisp_car(top_of_stack->rest);
				top_of_stack->subeval_arg = lisp_cons(lisp_car(top_of_stack->list), constant_nil);

				top_of_stack = stack_call(top_of_stack,
							  top_of_stack->env,
							  top_of_stack->subeval_arg);
				top_of_stack->return_path = 3;
				continue;
			}
		}
		switch (top_of_stack->return_path) {
		case 0:
			retval = top_of_stack->retval;
			stack_return(top_of_stack);
			return retval;
		case 1:
			retval = top_of_stack->retval;
			top_of_stack = stack_return(top_of_stack);
			top_of_stack->retval = retval;
			gc_unprotect(top_of_stack->sexpr);
			top_of_stack->p = lisp_cdr(top_of_stack->p);
			if (get_pair(top_of_stack->p) != get_pair(constant_nil)) {
				top_of_stack->sexpr = lisp_cons(lisp_car(top_of_stack->p), constant_nil);
				if (get_pair(lisp_cdr(top_of_stack->p)) == get_pair(constant_nil)) {
					top_of_stack->env = top_of_stack->call_env;
					top_of_stack->rest = top_of_stack->sexpr;
					continue;
				}
				top_of_stack = stack_call(top_of_stack,
							  top_of_stack->call_env,
							  top_of_stack->sexpr);
				top_of_stack->return_path = 1;
				continue;
			}
			environment_free(top_of_stack->call_env);
			gc_unprotect(top_of_stack->list);
			break;
		case 3:
			top_of_stack->prev->args = lisp_cons(top_of_stack->retval, constant_nil);
			top_of_stack = stack_return(top_of_stack);
			gc_unprotect(top_of_stack->subeval_arg);

			if (lisp_car(lisp_car(top_of_stack->args)).vp_opaque == opaque_builtin_function ||
			    lisp_car(lisp_car(top_of_stack->args)).vp_opaque == opaque_user_function) {
				top_of_stack->arglink = &get_pair(top_of_stack->args)->cdr;

				top_of_stack->list = lisp_cdr(top_of_stack->list);

				if (0) {
		case 4:
					element = lisp_cons(top_of_stack->retval, constant_nil);
					top_of_stack = stack_return(top_of_stack);
					gc_unprotect(top_of_stack->subeval_arg);

					*top_of_stack->arglink = element;
					top_of_stack->arglink = &get_pair(element)->cdr;

					gc_unprotect(element);
					top_of_stack->list = lisp_cdr(top_of_stack->list);
				}
				if (get_pair(top_of_stack->list) != get_pair(constant_nil)) {
					top_of_stack->subeval_arg = lisp_cons(lisp_car(top_of_stack->list),
									      constant_nil);

					top_of_stack = stack_call(top_of_stack,
								  top_of_stack->env,
								  top_of_stack->subeval_arg);
					top_of_stack->return_path = 4;
					continue;
				}
			} else {
				get_pair(top_of_stack->args)->cdr = lisp_cdr(top_of_stack->list);
			}
		case 5:
			top_of_stack->list = top_of_stack->args;
			top_of_stack->function = lisp_car(top_of_stack->list);
			top_of_stack->args = lisp_cdr(top_of_stack->list);

			closure = get_pair(top_of_stack->function);
			if (closure->car.vp_opaque == opaque_builtin_function ||
			    closure->car.vp_opaque == opaque_builtin_macro) {
				top_of_stack = stack_call(top_of_stack,
							  top_of_stack->env,
							  top_of_stack->args);
				top_of_stack->return_path = 6;
				top_of_stack->retval = (*closure->cdr.vp_builtin)(top_of_stack,
										  top_of_stack->prev->env,
										  top_of_stack->prev->args);
				break;
			} else if (closure->car.vp_opaque == opaque_user_function) {
				union value_pointer function_env;
				union value_pointer formal_args;
				union value_pointer function_body;
				struct environment *lambda_env;

				top_of_stack->p = closure->cdr;
				function_env = lisp_car(top_of_stack->p);
				formal_args = lisp_car(lisp_cdr(top_of_stack->p));
				function_body = lisp_cdr(lisp_cdr(top_of_stack->p));

				lambda_env = resource_get(&environments,
							  get_integer(function_env));
				top_of_stack->call_env = environment_new(lambda_env);
				for (top_of_stack->p = formal_args;
				     (pointer_type(top_of_stack->p) == 3 &&
				      get_pair(top_of_stack->p) != get_pair(constant_nil) &&
				      get_pair(top_of_stack->args) != get_pair(constant_nil));
				     top_of_stack->p = lisp_cdr(top_of_stack->p),
				     top_of_stack->args = lisp_cdr(top_of_stack->args)) {
					binding_new(top_of_stack->call_env,
						    get_charseq_chars(lisp_car(top_of_stack->p)),
						    lisp_car(top_of_stack->args));
				}
				if (pointer_type(top_of_stack->p) == 1) {
					binding_new(top_of_stack->call_env,
						    get_charseq_chars(top_of_stack->p),
						    top_of_stack->args);
				}

				top_of_stack->p = function_body;
				if (get_pair(top_of_stack->p) != get_pair(constant_nil)) {
					top_of_stack->sexpr = lisp_cons(lisp_car(top_of_stack->p), constant_nil);
					if (get_pair(lisp_cdr(top_of_stack->p)) == get_pair(constant_nil)) {
						top_of_stack->env = top_of_stack->call_env;
						top_of_stack->rest = top_of_stack->sexpr;
						continue;
					}
					top_of_stack = stack_call(top_of_stack,
								  top_of_stack->call_env,
								  top_of_stack->sexpr);
					top_of_stack->return_path = 1;
					continue;
				}
				environment_free(top_of_stack->call_env);
				break;
			}
			gc_unprotect(top_of_stack->list);
			break;
		case 6:
			retval = top_of_stack->retval;
			top_of_stack = stack_return(top_of_stack);
			top_of_stack->retval = retval;
			gc_unprotect(top_of_stack->list);
			break;
		case 7:
			top_of_stack->return_path = 8;
			top_of_stack->evaluating = 1;
			continue;
		case 8:
			retval = top_of_stack->retval;
			gc_unprotect(top_of_stack->rest);
			top_of_stack = stack_return(top_of_stack);

			consequent = lisp_car(lisp_cdr(lisp_cdr(lisp_car(top_of_stack->rest))));
			alternate = lisp_cdr(lisp_cdr(lisp_cdr(lisp_car(top_of_stack->rest))));
			if (pointer_type(retval) != 1 ||
			    retval.vp_charseq != constant_false.vp_charseq) {
				element = lisp_cons(consequent, constant_nil);
			} else if (get_pair(alternate) != get_pair(constant_nil)) {
				element = lisp_cons(lisp_car(alternate), constant_nil);
			} else {
				top_of_stack->retval = constant_nil;
				break;
			}

			gc_unprotect(retval);
			top_of_stack->rest = element;
			top_of_stack->evaluating = 1;
			continue;
		}
		top_of_stack->evaluating = 0;
	}
}

union value_pointer builtin_string_to_list(struct stack_slot *top_of_stack,
					   struct environment *env,
					   union value_pointer rest)
{
	union value_pointer retval;
	union value_pointer *v;
	char const *s;
	size_t i, n;

	s = get_charseq_chars(lisp_car(rest));
	n = get_charseq_len(lisp_car(rest));
	retval = lisp_cons(constant_nil, constant_nil);
	v = &get_pair(retval)->cdr;
	for (i = 0; i < n; i++) {
		*v = lisp_cons(lisp_integer(s[i]), constant_nil);
		gc_unprotect(*v);
		v = &get_pair(*v)->cdr;
	}
	v = &get_pair(retval)->cdr;
	gc_protect(*v);
	gc_unprotect(retval);
	retval = *v;

	return retval;
}

union value_pointer builtin_list_to_string(struct stack_slot *top_of_stack,
					   struct environment *env,
					   union value_pointer rest)
{
	union value_pointer retval;
	union value_pointer l;
	struct charseq *cs;
	int i, n;

	l = lisp_car(rest);
	n = lisp_length(l);
	retval = charseq_new("", 0, n, 2);
	cs = get_charseq(retval);
	for (i = 0; i < n; i++) {
		cs->s[i] = get_integer(lisp_car(l));
		l = lisp_cdr(l);
	}
	cs->s[i] = 0;

	return retval;
}

union value_pointer builtin_car(struct stack_slot *top_of_stack,
				struct environment *env,
				union value_pointer rest)
{
	return lisp_car(lisp_car(rest));
}

union value_pointer builtin_cdr(struct stack_slot *top_of_stack,
				struct environment *env,
				union value_pointer rest)
{
	return lisp_cdr(lisp_car(rest));
}

union value_pointer builtin_cons(struct stack_slot *top_of_stack,
				 struct environment *env,
				 union value_pointer rest)
{
	return gc_unprotect(lisp_cons(lisp_car(rest), lisp_car(lisp_cdr(rest))));
}

union value_pointer builtin_eq_p(struct stack_slot *top_of_stack,
				 struct environment *env,
				 union value_pointer rest)
{
	union value_pointer a, b;

	a = lisp_car(rest);
	b = lisp_car(lisp_cdr(rest));

	if (pointer_type(a) != pointer_type(b)) {
		return constant_false;
	}

	switch (pointer_type(a)) {
	case 0:
		if (get_integer(a) != get_integer(b)) {
			return constant_false;
		}
		break;
	case 1:
	case 2:
		if (get_charseq(a) != get_charseq(b)) {
			return constant_false;
		}
		break;
	case 3:
		if (a.vp_pair != b.vp_pair) {
			return constant_false;
		}
		break;
	}

	return constant_true;
}

union value_pointer builtin_display(struct stack_slot *top_of_stack,
				    struct environment *env,
				    union value_pointer rest)
{
	pretty_print(lisp_car(rest));

	return constant_true;
}

union value_pointer builtin_list_p_helper(union value_pointer v)
{
	if (pointer_type(v) == 3) {
		if (get_pair(v) == get_pair(constant_nil)) {
			return constant_true;
		} else {
			return builtin_list_p_helper(lisp_cdr(v));
		}
	} else {
		return constant_false;
	}
}

union value_pointer builtin_list_p(struct stack_slot *top_of_stack,
				   struct environment *env,
				   union value_pointer rest)
{
	return builtin_list_p_helper(lisp_car(rest));
}

union value_pointer builtin_length(struct stack_slot *top_of_stack,
				   struct environment *env,
				   union value_pointer rest)
{
	return lisp_integer(lisp_length(lisp_car(rest)));
}

union value_pointer builtin_define(struct stack_slot *top_of_stack,
				   struct environment *env,
				   union value_pointer rest)
{
	union value_pointer name_and_args, definition;
	union value_pointer lambda_args, function;

	name_and_args = lisp_car(rest);
	definition = lisp_car(lisp_cdr(rest));

	lambda_args = lisp_cons(lisp_cdr(name_and_args), lisp_cons(definition, constant_nil));

	function = builtin_lambda(top_of_stack, env, lambda_args);
	gc_unprotect(lambda_args);

	binding_new(env, get_charseq_chars(lisp_car(name_and_args)), function);

	return constant_nil;
}

union value_pointer builtin_if(struct stack_slot *top_of_stack,
			       struct environment *env,
			       union value_pointer rest)
{
	union value_pointer retval;
	union value_pointer condition, consequent, alternate;
	union value_pointer v;

	retval = constant_nil;

	condition = lisp_car(rest);
	consequent = lisp_car(lisp_cdr(rest));
	alternate = lisp_cdr(lisp_cdr(rest));

	v = lisp_cons(condition, constant_nil);

	top_of_stack->rest = v;
	top_of_stack->return_path = 7;
	return;

	condition = builtin_eval(top_of_stack, env, v);
	gc_unprotect(v);

	if (pointer_type(condition) != 1 ||
	    condition.vp_charseq != constant_false.vp_charseq) {
		v = lisp_cons(consequent, constant_nil);
	} else if (get_pair(alternate) != get_pair(constant_nil)) {
		v = lisp_cons(lisp_car(alternate), constant_nil);
	} else {
		return retval;
	}

	gc_unprotect(condition);
	retval = builtin_eval(top_of_stack, env, v);
	gc_unprotect(v);

	return retval;
}

union value_pointer builtin_lambda(struct stack_slot *top_of_stack,
				   struct environment *env,
				   union value_pointer rest)
{
	union value_pointer retval;

	retval = lisp_cons(lisp_opaque(opaque_user_function),
			   lisp_cons(lisp_integer(env->resource_num), rest));
	pointer_mark(&retval, 3);

	return retval;
}

union value_pointer builtin_quote(struct stack_slot *top_of_stack,
				  struct environment *env,
				  union value_pointer rest)
{
	return lisp_car(rest);
}

union value_pointer builtin_plus(struct stack_slot *top_of_stack,
				 struct environment *env,
				 union value_pointer rest)
{
	union value_pointer retval;
	int sum;

	for (sum = 0; get_pair(rest) != get_pair(constant_nil); rest = lisp_cdr(rest)) {
		sum += get_integer(lisp_car(rest));
	}

	retval.vp_int = sum << 2;
	pointer_mark(&retval, 0);

	return retval;
}

union value_pointer builtin_minus(struct stack_slot *top_of_stack,
				  struct environment *env,
				  union value_pointer rest)
{
	union value_pointer retval;
	union value_pointer lhs;
	int difference;

	lhs = lisp_car(rest);
	rest = lisp_cdr(rest);
	difference = get_integer(lhs);
	if (get_pair(rest) != get_pair(constant_nil)) {
		for (difference = get_integer(lhs);
		     get_pair(rest) != get_pair(constant_nil);
		     rest = lisp_cdr(rest)) {
			difference -= get_integer(lisp_car(rest));
		}
	} else {
		difference = -get_integer(lhs);
	}

	retval.vp_int = difference << 2;
	pointer_mark(&retval, 0);

	return retval;
}
void gc_mark(union value_pointer v)
{
	switch (pointer_type(v)) {
		struct pair *p;
	case 1:
	case 2:
		get_charseq(v)->gc |= 1;
		break;
	case 3:
		p = get_pair(v);
		if (p->gc & 1) {
			break;
		}
		p->gc |= 1;
		if (p->car.vp_opaque != opaque_builtin_function &&
		    p->car.vp_opaque != opaque_builtin_macro) {
			if (p->car.vp_opaque != opaque_user_function &&
			    p->car.vp_opaque != opaque_user_macro) {
				gc_mark(lisp_car(v));
			}
			gc_mark(lisp_cdr(v));
		}
		break;
	}
}

void gc_protect(union value_pointer v)
{
	switch (pointer_type(v)) {
		struct pair *p;
	case 1:
	case 2:
		get_charseq(v)->gc |= 2;
		break;
	case 3:
		p = get_pair(v);
		p->gc |= 2;
		break;
	}
	gc_mark(v);
}

union value_pointer gc_unprotect(union value_pointer v)
{
	switch (pointer_type(v)) {
		struct pair *p;
	case 1:
	case 2:
		get_charseq(v)->gc &= ~2;
		break;
	case 3:
		p = get_pair(v);
		p->gc &= ~2;
		break;
	}

	return v;
}

void gc_unmark_all(void)
{
	int j;

	for (j = 0; j < resource_count(&charseqs); j++) {
		struct charseq *cs = resource_get(&charseqs, j);
		if (cs) {
			cs->gc &= ~1;
		}
	}
	for (j = 0; j < resource_count(&pairs); j++) {
		struct pair *p = resource_get(&pairs, j);
		if (p) {
			p->gc &= ~1;
		}
	}
	for (j = 0; j < resource_count(&pairs); j++) {
		struct pair *p = resource_get(&pairs, j);
		if (p && p->gc) {
			if (p->car.vp_opaque != opaque_builtin_function &&
			    p->car.vp_opaque != opaque_builtin_macro) {
				if (p->car.vp_opaque != opaque_user_function &&
				    p->car.vp_opaque != opaque_user_macro) {
					gc_mark(p->car);
				}
				gc_mark(p->cdr);
			}
		}
	}
}

void gc_collect(struct environment *env)
{
	int j;

	gc_unmark_all();

	for (; env; env = env->parent) {
		for (j = 0; j < resource_count(&env->variables); j++) {
			struct binding *b = resource_get(&env->variables, j);

			if (b) {
				gc_mark(b->name);
				gc_mark(b->value);
			}
		}
	}

	for (j = 0; j < resource_count(&charseqs); j++) {
		struct charseq *cs = resource_get(&charseqs, j);
		if (cs && cs->gc == 0) {
			free(cs->s);
			resource_free(&charseqs, j);
		}
	}
	for (j = 0; j < resource_count(&pairs); j++) {
		struct pair *p = resource_get(&pairs, j);
		if (p && p->gc == 0) {
			resource_free(&pairs, j);
		}
	}
}

int main()
{
	char *buf = NULL;
	size_t bufused = 0;
	size_t bufsize = 0;
	size_t formsize;
	union value_pointer i, form;
	struct environment *top_env;
	int j;

	resource_pool_init(&bindings, sizeof (struct binding));
	resource_pool_init(&charseqs, sizeof (struct charseq));
	resource_pool_init(&environments, sizeof (struct environment));
	resource_pool_init(&pairs, sizeof (struct pair));
	resource_pool_init(&stack_slots, sizeof (struct stack_slot));

	constant_nil = pair_new();
	get_pair(constant_nil)->car = constant_nil;
	get_pair(constant_nil)->cdr = constant_nil;

	while (!feof(stdin) && !ferror(stdin)) {
		size_t n;

		if (bufused >= bufsize) {
			buf = realloc(buf, bufsize * 2 + 16);
			if (buf == NULL) {
				abort();
			}
			bufsize = bufsize * 2 + 16;
		}
		n = fread(buf + bufused, 1, bufsize - bufused, stdin);
		bufused += n;
	}

	top_env = environment_new(NULL);

	constant_true = charseq_new("#t", 2, 2, 1);
	constant_false = charseq_new("#f", 2, 2, 1);

	binding_new(top_env, "#t", constant_true);
	binding_new(top_env, "#f", constant_false);

	binding_new(top_env, "car", builtin_new(opaque_builtin_function, builtin_car));
	binding_new(top_env, "cdr", builtin_new(opaque_builtin_function, builtin_cdr));
	binding_new(top_env, "cons", builtin_new(opaque_builtin_function, builtin_cons));
	binding_new(top_env, "eq?", builtin_new(opaque_builtin_function, builtin_eq_p));
	binding_new(top_env, "display", builtin_new(opaque_builtin_function, builtin_display));
	binding_new(top_env, "list?", builtin_new(opaque_builtin_function, builtin_list_p));
	binding_new(top_env, "length", builtin_new(opaque_builtin_function, builtin_length));
	binding_new(top_env, "primitive-eval", builtin_new(opaque_builtin_function, builtin_eval));
	binding_new(top_env, "string->list", builtin_new(opaque_builtin_function, builtin_string_to_list));
	binding_new(top_env, "list->string", builtin_new(opaque_builtin_function, builtin_list_to_string));

	binding_new(top_env, "define", builtin_new(opaque_builtin_macro, builtin_define));
	binding_new(top_env, "if", builtin_new(opaque_builtin_macro, builtin_if));
	binding_new(top_env, "lambda", builtin_new(opaque_builtin_macro, builtin_lambda));
	binding_new(top_env, "quote", builtin_new(opaque_builtin_macro, builtin_quote));

	binding_new(top_env, "+", builtin_new(opaque_builtin_function, builtin_plus));
	binding_new(top_env, "-", builtin_new(opaque_builtin_function, builtin_minus));

	form = lisp_cons(constant_nil, constant_nil);

	formsize = parse_forms(buf, bufused, &get_pair(form)->cdr);
	i = form;
	form = lisp_cdr(form);
	gc_protect(form);
	gc_unprotect(i);

	if (formsize != bufused) {
		printf("this stuff left over: \"%.*s\"\n",
		       bufused - formsize, buf + formsize);
	}

	for (i = form; get_pair(i) != get_pair(constant_nil); i = form) {
		union value_pointer rest, retval;

		rest = lisp_cons(lisp_car(i), constant_nil);

		pretty_print(lisp_car(i));
		printf(" -> ");
		retval = builtin_eval(NULL, top_env, rest);
		gc_unprotect(rest);
		pretty_print(retval);
		gc_unprotect(retval);
		printf("\n");

		form = lisp_cdr(i);
		gc_protect(form);
		gc_unprotect(i);

		gc_collect(top_env);
	}

	environment_free(top_env);

	gc_unprotect(constant_nil);
	gc_collect(NULL);

	resource_pool_free(&charseqs);
	resource_pool_free(&pairs);
	free(bindings.resources);
	free(environments.resources);

	free(buf);

	return 0;
}
