SERIAL_BASE	equ	0x3f8

THR		equ	0
RBR		equ	0
DLL		equ	0
DLM		equ	1
IER		equ	1
IIR		equ	2
FCR		equ	2
LCR		equ	3
MCR		equ	4
LSR		equ	5
MSR		equ	6
SCR		equ	7

DEFAULT_DLL	equ	1
DEFAULT_DLM	equ	0

start:
		jmp	init_port

port_data:
		db	LCR, 0x80
		db	DLL, DEFAULT_DLL
		db	DLM, DEFAULT_DLM
		db	LCR, 0x03
		db	IER, 0x00
		db	FCR, 0x00
		db	MCR, 0x03
		; send a chirp
		db	THR, 0x55
		db	0xff

init_port:
		mov	cx, SERIAL_BASE
		call	.get_ip
.get_ip:
		pop	si
		sub	si, .get_ip-port_data
.next_port:
		lodsb
		mov	dx, cx
		add	dl, al
		jc	.done
		lodsb
		out	dx, al
		jmp	short .next_port
.done:

mainloop:
		call	getchar
		cmp	al, 'e'
		je	enter_bytes 
		cmp	al, 'g'
		je	call_sub
		mov	al, '?'
		call	putchar
		jmp	short mainloop

enter_bytes:
		mov	cx, 4
		call	get_hexint
		mov	di, ax
.next_byte:
		mov	cx, 2
		call	get_hexint
		jc	mainloop
		stosb
		jmp	short .next_byte

get_hexint:
		mov	bx, 0
.next_digit:
		sub	cx, 1
		jz	.done

		call	getchar
		cmp	al, 012
		je	done
		cmp	al, 033
		je	.abort
		call	putchar
		or	al, 0x60 ; [optional] case-insensitive
		mov	ah, 'a'-10
		cmp	al, 'f'
		ja	.digit
		mov	ah, 'p'
.digit:
		sub	al, ah
		mov	ah, 0
		shl	bx, 1
		shl	bx, 1
		shl	bx, 1
		shl	bx, 1
		add	bx, ax
		jmp	short .next_digit
.done:
		mov	ax, 0
		add	ax, bx
		ret
.abort:
		stc
		ret

getchar:
		mov	dx, SERIAL_BASE+LSR
.spin:
		in	al, dx
		and	al, 1
		jz	.spin
		mov	dx, SERIAL_BASE+RBR
		in	al, dx
		ret

putchar:
		mov	dx, SERIAL_BASE+LSR
		mov	ah, al
.spin:
		in	al, dx
		and	al, 0x20
		jz	.spin
		mov	dx, SERIAL_BASE+RBR
		mov	al, ah
		out	dx, al
		ret
; vim: set ft=nasm: ;
